# Tutorial_1: #

## Setup: ##
### 1. Maven: ###
    a. url: http://www-us.apache.org/dist/maven/maven-3/3.5.0/binaries/apache-maven-3.5.0-bin.zip
    b. version: 3.5
    installation:
    -------------
    i. Download-->extract in c:/programfiles--> add this path in environment variables.


### 2. jdk 1.8_u_XXXXX ###
    a. url: http://download.oracle.com/otn-pub/java/jdk/8u144-b01/090f390dda5b47b9b721c7dfaa008135/jdk-8u144-windows-x64.exe
    installation:
    -------------
    i. Download-->run exe to install --> add installed jdk path in environment variables.

### 3.  Eclipse: ###
    a. url: https://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/oxygen/R/eclipse-jee-oxygen-R-win32-x86_64.zip
    b. version: neon


# Assignment: #
    1. Setup environment by installing above steps
    2. Create 5 maven projects
    3. Create one class in each project printing one line.


